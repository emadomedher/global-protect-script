# Global Protect Script

This script is meant to be used instead of the Global Protect App and depends on openconnect.

## Install

Add the following to .bashrc
```
export PATH=$PATH:absolute/path/to/bin
```
Replacing it with the path to the script.

## License

GPL v3 - https://www.gnu.org/licenses/gpl-3.0.en.html